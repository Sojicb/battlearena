# README #

* Author: Bogoljub Sojic
* Project Name: Battle Arena
* Date: 07.10.2019.

### Tehnologies Used ###

* Visual Studio 2019
* SQL Server Managment Studio
* SQL Server 2017

### How do I get set up? ###

* For This application you will need SQL Server Managment Studio and Visual Studio
* Open DevBattleArenaDB.bacpac in SSMS
* Run The code in Visual Studio

### How to Play? ###

* There are Two Battle modes to choose from. First is Player vs Player combat and the second one is Player vs AI where you play against a computer.
* After Choosing a Battle mode, you proceed to pick your Hero. There are four Heros with their decriptions to Choose from.
* There are also two battle Arenas. Each Battle Arena has it's own Advantage and Disadvantage.
* After you have choosen everything, the game begins. Each Player can cast one Ability each round and the Game ends when one Player is below Zero health.