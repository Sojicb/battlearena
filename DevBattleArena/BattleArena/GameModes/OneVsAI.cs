﻿using BattleArena.ArenaModels;
using BattleArena.Buffs;
using BattleArena.Debuffs;
using BattleArena.Logs;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using static BattleArena.Hero;

namespace BattleArena.GameModes
{
    public class OneVsAI : IGameMode
    {
        Random random = new Random();

        public int numberOfRounds;
        public OneVsAI(Hero playerOne, Hero playerTwo, Arena arena)
        {
            PlayerOne = playerOne;
            PlayerTwo = playerTwo;
            Arena = arena;
        }

        public Hero PlayerOne { get; }
        public Hero PlayerTwo { get; }
        public Arena Arena { get; }

        public void PlayGame()
        {
            while (!IsPlayerDead())
            {
                Console.WriteLine("================================================== Round " + numberOfRounds + " ==================================================");
                ClearBuff(PlayerOne);
                ClearDebuff(PlayerOne);
                Tick(PlayerOne);
                CastSpellAI(PlayerOne, PlayerTwo);

                if (IsPlayerDead())
                {
                    break;
                }

                ClearBuff(PlayerTwo);
                ClearDebuff(PlayerTwo);
                Tick(PlayerTwo);
                CastSpell(PlayerTwo, PlayerOne);
                Console.WriteLine("============================================= End of Round " + numberOfRounds + " ===============================================");
                numberOfRounds++;
            }
            Winner();
            CombatLogs();
        }

        private void Winner()
        {
            if (PlayerOne.CurrentHealth <= 0)
            {
                Console.WriteLine(PlayerTwo.Name + " Won!");
            }
            else
            {
                Console.WriteLine(PlayerOne.Name + " Won!");
            }
        }

        private void CombatLogs()
        {
            BattleLog battleLog = new BattleLog();

            battleLog.PlayerOne = PlayerOne.Name;
            battleLog.PlayerTwo = PlayerTwo.Name;
            battleLog.ArenaName = Arena.Name;
            battleLog.Rounds = numberOfRounds;
            battleLog.PlayerOneDamageDealt = PlayerOne.DamageDone;
            battleLog.PlayerOneDamageTaken = PlayerOne.DamageTaken;
            battleLog.PlayerOneHealingDone = PlayerOne.HealingDone;
            battleLog.PlayerTwoDamageDealt = PlayerTwo.DamageDone;
            battleLog.PlayerTwoDamageTaken = PlayerTwo.DamageTaken;
            battleLog.PlayerTwoHealingDone = PlayerTwo.HealingDone;

            string result = JsonConvert.SerializeObject(battleLog);
            string path = @"D:\log.json";

            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine("\n" + result.ToString());
                streamWriter.Close();
            }
        }

        private void Tick(Hero hero)
        {
            foreach (Buff buff in hero.Buffs)
            {
                buff.Action(hero);
            }
            foreach (Debuff debuff in hero.Debuffs)
            {
                debuff.Action(hero);
            }
        }
        private void ClearBuff(Hero hero)
        {
            Buff buff = hero.Buffs.FirstOrDefault(x => x.CurrentTick == x.Duration);
            hero.Buffs.Remove(buff);
        }
        private void ClearDebuff(Hero hero)
        {
            Debuff debuff = hero.Debuffs.FirstOrDefault(x => x.CurrentTick == x.Duration);
            hero.Debuffs.Remove(debuff);
        }

        private void CastSpell(Hero caster, Hero opponent)
        {
            SpellReport spellReport = SpellReport.SUCCESS;
            do
            {
                if(spellReport == SpellReport.SUCCESS)
                {
                    spellReport = caster.CastSpell(opponent);
                }
                else
                {
                    Console.WriteLine("Can't cast that spell yet!");
                    spellReport = caster.CastSpell(opponent, true);
                }
            } while (spellReport == SpellReport.FAILURE);
        }

        private void CastSpellAI(Hero caster, Hero opponent)
        {
            SpellReport spellReport = SpellReport.SUCCESS;
            do
            {
                if (spellReport == SpellReport.SUCCESS)
                {
                    spellReport = caster.CastSpellAI(opponent, false, random.Next(1, 5));
                }
                else
                {
                    Console.WriteLine("Can't cast that spell yet!");
                    spellReport = caster.CastSpellAI(opponent, true, random.Next(1, 5));
                }
            } while (spellReport == SpellReport.FAILURE);
        }

        private bool IsPlayerDead()
            => PlayerOne.CurrentHealth <= 0 || PlayerTwo.CurrentHealth <= 0;
    }
}
