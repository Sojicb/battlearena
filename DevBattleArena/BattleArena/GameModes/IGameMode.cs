﻿namespace BattleArena.GameModes
{
    public interface IGameMode
    {
        void PlayGame();
    }
}
