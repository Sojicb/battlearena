﻿using BattleArena.ArenaModels;
using BattleArena.ArenaModels.Arenas;
using BattleArena.DataAccess;
using BattleArena.HeroModels;
using System;

namespace BattleArena.GameModes
{
	public class ChooseMode
    {

        public static IGameMode ChooseGameMode()
        {
            Console.WriteLine("Welcome to Battle Arena, Choose your mode: ");
            Console.WriteLine("Press 1 For One Vs One. Play Against your friends (or yourself) in a Player versus Player Combat.");
            Console.WriteLine("Press 2 for One Vs AI. Play Against a Computer.");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    return new OneVsOne(ChooseHero(), ChooseHero(), ChooseArena());
                case 2:
                    return new OneVsAI(GetRandomOpponent(), ChooseHero(), ChooseArena());
            }
            return null;
        }

        
        private static Hero ChooseHero()
        {
            DataBaseAccess dataBaseAccess = new DataBaseAccess();

            Console.WriteLine("Press 1 to Choose Warrior. Warrior is a melee Hero who uses Rage for his Attacks. Very powerful against Debuffs.");
            Console.WriteLine("Press 2 to Choose Paladin. With both Protection and Harmful spells, Paladin is the perfect solution against everyone.");
            Console.WriteLine("Press 3 to Choose Death Knight. Master of Runes and Blades, Death Knight is the most dangerous opponent in the Arena.");
            Console.WriteLine("Press 4 to Choose Sorcerer. What Sorcerer lacks in Health and Buffs, he compensates in Arcane Magic.");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    Warrior warrior = new Warrior(dataBaseAccess.HeroRepository.Get(1));
                    return warrior;
                case 2:
                    Paladin paladin = new Paladin(dataBaseAccess.HeroRepository.Get(2));
                    return paladin;
                case 3:
                    DeathKnight deathKnight = new DeathKnight(dataBaseAccess.HeroRepository.Get(3));
                    return deathKnight;
                case 4:
                    Sorcerer sorcerer = new Sorcerer(dataBaseAccess.HeroRepository.Get(4));
                    return sorcerer;
            }
            return null;
        }

        public static Hero GetRandomOpponent()
        {
            DataBaseAccess dataBaseAccess = new DataBaseAccess();
            Random random = new Random();

            switch (random.Next(1,4))
            {
                case 1:
                    Warrior warrior = new Warrior(dataBaseAccess.HeroRepository.Get(1));
                    Console.WriteLine("Computer picked Warrior as their Hero.");
                    return warrior;
                case 2:
                    Paladin paladin = new Paladin(dataBaseAccess.HeroRepository.Get(2));
                    Console.WriteLine("Computer picked Paladin as their Hero.");
                    return paladin;
                case 3:
                    DeathKnight deathKnight = new DeathKnight(dataBaseAccess.HeroRepository.Get(3));
                    Console.WriteLine("Computer picked Death Knight as their Hero.");
                    return deathKnight;
                case 4:
                    Sorcerer sorcerer = new Sorcerer(dataBaseAccess.HeroRepository.Get(4));
                    Console.WriteLine("Computer picked Sorcerer as their Hero.");
                    return sorcerer;
            }
            return null;
        }

        private static Arena ChooseArena()
        {
            DataBaseAccess dataBaseAccess = new DataBaseAccess();
            Random random = new Random();

            Warsong warsong = new Warsong(dataBaseAccess.ArenaRepository.Get("Warsong"));
            TigersPeak tigersPeak = new TigersPeak(dataBaseAccess.ArenaRepository.Get("TigersPeak"));

            Console.WriteLine("Press 1 to Choose Warsong Arena. Health Regenerates every round but watch out for those bricks falling down. " + warsong.Description);
            Console.WriteLine("Press 2 to Choose Tiger's Peak Arena. Each Round your blade wears out more and more.");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    return warsong;
                case 2:
                    return tigersPeak;
            }
            return null;
        }

    }
}
