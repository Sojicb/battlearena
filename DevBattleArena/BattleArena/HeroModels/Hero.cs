﻿using BattleArena.Buffs;
using BattleArena.Debuffs;
using BattleArena.HeroModels;
using System.Collections.Generic;

namespace BattleArena
{
    public abstract class Hero
    {
        public enum SpellReport
        {
            SUCCESS, FAILURE
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int MaxHealth { get; set; }
        public int CurrentHealth { get; set; }
        public int MaxMana { get; set; }
        public int CurrentMana { get; set; }
        public int MaxDamage { get; set; }
        public int MinDamage { get; set; }
        public double HealthRegen { get; set; }
        public double ManaRegen { get; set; }
        public int DamageDone { get; set; }
        public int HealingDone { get; set; }
        public int DamageTaken { get; set; }
        public List<Buff> Buffs { get; set; }
        public List<Debuff> Debuffs { get; set; }

        public abstract SpellReport CastSpell(Hero opponent, bool isRecasted = false);
        public abstract SpellReport CastSpellAI(Hero opponent, bool isRecasted = false, int key = 0);

        public abstract SpellReport SpellOne(Hero opponent);
        public abstract SpellReport SpellTwo(Hero opponent);
        public abstract SpellReport SpellThree(Hero opponent);
        public abstract SpellReport SpellFour(Hero opponent);
        public abstract SpellReport SpellFive(Hero opponent);
    }
}
