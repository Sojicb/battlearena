﻿using BattleArena.Buffs;
using BattleArena.Buffs.DeathKnightBuffs;
using BattleArena.DataAccess.Models;
using BattleArena.Debuffs;
using BattleArena.Debuffs.DeathKnightDebuff;
using System;
using System.Collections.Generic;

namespace BattleArena.HeroModels
{
    public class DeathKnight : Hero
    {
        Random random = new Random();
        public int Runes = 0;
        

        public DeathKnight(HeroModel heroModels)
        {
            Id = heroModels.Id;
            Name = heroModels.Name;
            MaxHealth = heroModels.Health;
            CurrentHealth = heroModels.Health;
            CurrentMana = heroModels.Mana;
            MaxMana = heroModels.Mana;
            MaxDamage = heroModels.MaxDamage;
            MinDamage = heroModels.MinDamage;
            Buffs = new List<Buff>();
            Debuffs = new List<Debuff>();
        }

        public override SpellReport CastSpell(Hero opponent, bool isRecasted = false)
        {
            DisplayStats();
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    return SpellOne(opponent);
                case 2:
                    return SpellTwo(opponent);
                case 3:
                    return SpellThree(opponent);
                case 4:
                    return SpellFour(opponent);
                case 5:
                    return SpellFive(opponent);
            }

            return SpellReport.FAILURE;
        }
        public override SpellReport CastSpellAI(Hero opponent, bool isRecasted = false, int key = 0)
        {
            DisplayStats();
            switch (key)
            {
                case 1:
                    return SpellOne(opponent);
                case 2:
                    return SpellTwo(opponent);
                case 3:
                    return SpellThree(opponent);
                case 4:
                    return SpellFour(opponent);
                case 5:
                    return SpellFive(opponent);
            }
            return SpellReport.FAILURE;
        }
        private void DisplayStats()
        {
            Console.WriteLine("\n\nName: " + Name);
            Console.WriteLine("Health: " + CurrentHealth);
            Console.WriteLine("Mana: " + CurrentMana);
            Console.WriteLine("Runes: " + Runes);
            Console.WriteLine("*************************************");
            Console.WriteLine("Press 1 for Basic Attack" + "(Deals Damage between " + MinDamage + " and " + MaxDamage + " to an Opponent. It generates one Rune per Attack.)");
            Console.WriteLine("Press 2 for Death Strike" + "(Death Knight Strikes an enemy dealing damage equal to his Basic Attack and Heals himself for 50 - 100 Health. This Attack generates two Runes. Tip: Use This Attack While Buff Bloody Runeblade is Activated.)");
            Console.WriteLine("Press 3 for Heart Strike" + "(Striking his enemy for damage equal to his Basic Attack times two and applying Bleed Debuff that hits the enemy for 20 Health each Round. This Spell Uses 2 Runes.)");
            Console.WriteLine("Press 4 for Vampiric Blood" + "(Death Knight Buffs himself, gaining 50 Health and one Rune each round. This spell uses 2 Runes.)");
            Console.WriteLine("Press 5 for Bloody Runeblade" + "(Death Knight Empowers his Blade for 75 bonus Damage for next three Attacks! Uses three Runes.)");
            Console.WriteLine("*************************************");
            Console.WriteLine("Cast Your Spell: ");
        }

        public override SpellReport SpellOne(Hero opponent)
        {
            Console.WriteLine("Casting Basic Attack");
            int damage = random.Next(MinDamage, MaxDamage);
            if (Runes < 6)
            {
                Runes++;
            }
            opponent.CurrentHealth -= damage;
            Console.WriteLine(Name + " Hits for " + damage + " Damage");
            Console.WriteLine(opponent.Name + " Current Health: " + opponent.CurrentHealth);
            DamageDone += damage;
            opponent.DamageTaken += damage;

            return SpellReport.SUCCESS;
        }
        public override SpellReport SpellTwo(Hero opponent)
        {
            Console.WriteLine("Casting Death Strike");
            int damage = random.Next(MinDamage, MaxDamage);
            int heal = random.Next(50, 100);
            if (Runes < 6)
            {
                Runes += 2;
            }
            opponent.CurrentHealth -= damage;
            CurrentHealth = CurrentHealth + heal > MaxHealth ? MaxHealth : CurrentHealth + heal;
            Console.WriteLine(Name + " Hits for " + damage + " Damage");
            Console.WriteLine(Name + " Heals for " + heal + " Health");
            Console.WriteLine(Name + " Current Health: " + CurrentHealth);
            Console.WriteLine(opponent.Name + " Remaining Health: " + opponent.CurrentHealth);
            DamageDone += damage;
            opponent.DamageTaken += damage;
            HealingDone += heal;

            return SpellReport.SUCCESS;
        }
        public override SpellReport SpellThree(Hero opponent)
        {
            int damage;
            if (Runes >= 2)
            {
                damage = random.Next(MinDamage, MaxDamage) * 2;
                Console.WriteLine("Casting Heart Strike");
                opponent.CurrentHealth -= damage;
                opponent.Debuffs.Add(new Bleed());
                Runes -= 2;
                Console.WriteLine(Name + " Hits for " + damage + " Damage");
                Console.WriteLine(opponent.Name + " Current Health: " + opponent.CurrentHealth);
                DamageDone += damage;
                opponent.DamageTaken += damage;

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }
        public override SpellReport SpellFour(Hero opponent)
        {
            if(Runes >= 2)
            {
                Console.WriteLine("Casting Vampiric Blood");
                Buffs.Add(new BloodRunes(this));

                Runes -= 2;
                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }
        public override SpellReport SpellFive(Hero opponent)
        {
            Console.WriteLine("Casting Bloody Runeblade");
            if(Runes >= 3)
            {
                Buffs.Add(new RuneBlade());
                Runes -= 3;

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }
    }
}
