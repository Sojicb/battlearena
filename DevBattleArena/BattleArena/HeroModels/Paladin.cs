﻿using BattleArena.Buffs;
using BattleArena.Buffs.PaladinBuffs;
using BattleArena.DataAccess.Models;
using BattleArena.Debuffs;
using BattleArena.Debuffs.Paladin;
using System;
using System.Collections.Generic;

namespace BattleArena.HeroModels
{
    public class Paladin : Hero
    {
        Random random = new Random();
        public int Charges;

        public Paladin(HeroModel heroModels)
        {
            Id = heroModels.Id;
            Name = heroModels.Name;
            MaxHealth = heroModels.Health;
            CurrentHealth = heroModels.Health;
            CurrentMana = heroModels.Mana;
            MaxMana = heroModels.Mana;
            MaxDamage = heroModels.MaxDamage;
            MinDamage = heroModels.MinDamage;
            Buffs = new List<Buff>();
            Debuffs = new List<Debuff>();
        }

        public override SpellReport CastSpell(Hero opponent, bool isRecasted = false)
        {
            DisplayStats();
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    return SpellOne(opponent);
                case 2:
                    return SpellTwo(opponent);
                case 3:
                    return SpellThree(opponent);
                case 4:
                    return SpellFour(opponent);
                case 5:
                    return SpellFive(opponent);
            }

            return SpellReport.FAILURE;
        }

        public override SpellReport CastSpellAI(Hero opponent, bool isRecasted = false, int key = 0)
        {
            DisplayStats();
            switch (key)
            {
                case 1:
                    return SpellOne(opponent);
                case 2:
                    return SpellTwo(opponent);
                case 3:
                    return SpellThree(opponent);
                case 4:
                    return SpellFour(opponent);
                case 5:
                    return SpellFive(opponent);
            }
            return SpellReport.FAILURE;
        }

        private void DisplayStats()
        {
            Console.WriteLine("\n\nName: " + Name);
            Console.WriteLine("Health: " + CurrentHealth);
            Console.WriteLine("Mana: " + CurrentMana);
            Console.WriteLine("Paladins Charges: " + Charges);
            Console.WriteLine("*************************************");
            Console.WriteLine("Press 1 for Basic Attack" + "(Deals Damage between " + MinDamage + " and " + MaxDamage + " to an Opponent. It requires no mana and generates 1 Charge.)");
            Console.WriteLine("Press 2 for Judgement" + "(Paladin Hits his opponent with his Hammer dealing 150 Damage. This Attack also activates a debuff that deals 15 damage to opponent per round (Lasts 4 rounds). This attack uses 50 mana and generates 1 Charge.)");
            Console.WriteLine("Press 3 for Divine Light" + "(Paladin restores 40 - 60 Mana and activates buff that Heals him 50 Health for next three rounds. This spell is Free!)");
            Console.WriteLine("Press 4 for Holy Light" + "(Paladin Heals himself for 200 Health and costs 90 Mana. If three Charges are generated, the spell is free and grants Paladin 500 Health! )");
            Console.WriteLine("Press 5 for CrusaderStrike" + "(Depending on Charges generated, paladin hits his opponent for: 50 - 100 for 0 Charges / 100 - 150 for 1 Charge / 150 - 200 for 2 Charges / 200 - 250 for 3 Charges. This spell costs 70 Mana)");
            Console.WriteLine("*************************************");
            Console.WriteLine("Cast Your Spell: ");
        }

        public override SpellReport SpellOne(Hero opponent)
        {
            Console.WriteLine("Casting Basic Attack");
            int damage = random.Next(MinDamage, MaxDamage);
            if (Charges < 3)
            {
                Charges++;
            }
            opponent.CurrentHealth -= damage;
            Console.WriteLine(Name + " Hits for " + damage);
            Console.WriteLine(opponent.Name + " Remaining Health: " + opponent.CurrentHealth);
            DamageDone += damage;
            opponent.DamageTaken += damage;

            return SpellReport.SUCCESS;
        }

        public override SpellReport SpellTwo(Hero opponent)
        {
            if (CurrentMana >= 50)
                {
                    Console.WriteLine("Casting Judgement");
                    if (Charges < 3)
                    {
                        Charges++;
                    }
                    opponent.CurrentHealth -= 150;
                    opponent.Debuffs.Add(new Judgement());
                    CurrentMana -= 50;

                    Console.WriteLine(Name + " Hits for " + 150 + " Damage");
                    Console.WriteLine(opponent.Name + " Remaining Health: " + opponent.CurrentHealth);
                    DamageDone += 150;
                    opponent.DamageTaken += 150;

                    return SpellReport.SUCCESS;
                }

            return SpellReport.SUCCESS;
        }

        public override SpellReport SpellThree(Hero opponent)
        {
            Console.WriteLine("Casting Divine Light");
            int ManaRegen;
            ManaRegen = random.Next(40, 60);
            CurrentMana = (CurrentMana + ManaRegen > MaxMana) ? MaxMana : CurrentMana + ManaRegen;
            Console.WriteLine("Paladin Restored Mana for " + ManaRegen + " Mana. Paladin's Mana: " + CurrentMana);
            Buffs.Add(new Rejuvenate());

            return SpellReport.SUCCESS;
        }

        public override SpellReport SpellFour(Hero opponent)
        {
            if (Charges == 3)
            {
                Console.WriteLine("Casting Holy Light");
                CurrentHealth = (CurrentHealth + 500 > MaxHealth) ? MaxHealth : CurrentHealth + 500;
                Charges = 0;
                Console.WriteLine("Paladins Current Health : " + CurrentHealth);
                HealingDone += 500;

                return SpellReport.SUCCESS;
            }
            if (CurrentMana >= 90)
            {
                Console.WriteLine("Casting Holy Light");
                CurrentHealth = (CurrentHealth + 200 > MaxHealth) ? MaxHealth : CurrentHealth + 200;
                CurrentMana = (CurrentMana - 90 <= 0) ? 0 : CurrentMana - 90;
                Console.WriteLine("Paladins Current Health : " + CurrentHealth);
                HealingDone += 200;

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }

        public override SpellReport SpellFive(Hero opponent)
        {
            if (CurrentMana >= 70)
            {
                int damage;
                Console.WriteLine("Casting Crusader Strike");
                if (Charges == 1)
                {
                    CrusaderStrike(opponent);
                }
                else if (Charges == 2)
                {
                    CrusaderStrike(opponent);
                }
                else if (Charges == 3)
                {
                    CrusaderStrike(opponent);
                }
                else
                {
                    damage = random.Next(50, 100);
                    opponent.CurrentHealth -= damage;
                    Console.WriteLine(Name + " Hits for " + damage + " Damage");
                    DamageDone += damage;
                    opponent.DamageTaken += damage;
                    Charges = 0;
                }
                CurrentMana -= 70;
                Console.WriteLine(opponent.Name + " Current Health: " + opponent.CurrentHealth);

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }

        private void CrusaderStrike(Hero opponent)
        {
            int damage = random.Next(100, 150) * Charges;
            opponent.CurrentHealth -= damage;
            Console.WriteLine(Name + " Hits for " + damage + " Damage");
            DamageDone += damage;
            opponent.DamageTaken += damage;
            Charges = 0;
        }
    }
}

