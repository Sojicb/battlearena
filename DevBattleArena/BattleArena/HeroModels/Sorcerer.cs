﻿using BattleArena.Buffs;
using BattleArena.DataAccess.Models;
using BattleArena.Debuffs;
using BattleArena.Debuffs.Sorcerer;
using System;
using System.Collections.Generic;

namespace BattleArena.HeroModels
{
    public class Sorcerer : Hero
    {
        Random random = new Random();
        private int arcaneStacks = 0;

        public Sorcerer(HeroModel heroModels)
        {
            Id = heroModels.Id;
            Name = heroModels.Name;
            MaxHealth = heroModels.Health;
            CurrentHealth = heroModels.Health;
            CurrentMana = heroModels.Mana;
            MaxMana = heroModels.Mana;
            MaxDamage = heroModels.MaxDamage;
            MinDamage = heroModels.MinDamage;
            Buffs = new List<Buff>();
            Debuffs = new List<Debuff>();
        }

        public override SpellReport CastSpell(Hero opponent, bool isRecasted = false)
        {
            DisplayStats();
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    return SpellOne(opponent);
                case 2:
                    return SpellTwo(opponent);
                case 3:
                    return SpellThree(opponent);
                case 4:
                    return SpellFour(opponent);
                case 5:
                    return SpellFive(opponent);
            }

            return SpellReport.FAILURE;
        }

        public override SpellReport CastSpellAI(Hero opponent, bool isRecasted = false, int key = 0)
        {
            DisplayStats();
            switch (key)
            {
                case 1:
                    return SpellOne(opponent);
                case 2:
                    return SpellTwo(opponent);
                case 3:
                    return SpellThree(opponent);
                case 4:
                    return SpellFour(opponent);
                case 5:
                    return SpellFive(opponent);
            }
            return SpellReport.FAILURE;
        }

        private void DisplayStats()
        {
            Console.WriteLine("\n\nName: " + Name);
            Console.WriteLine("Health: " + CurrentHealth);
            Console.WriteLine("Mana: " + CurrentMana);
            Console.WriteLine("Arcane Stacks: " + arcaneStacks);
            Console.WriteLine("*************************************");
            Console.WriteLine("Press 1 for Basic Attack" + "(Deals Damage between " + MinDamage + " and " + MaxDamage + " to an Opponent. It requires no mana and generates one Arcane Stack.)");
            Console.WriteLine("Press 2 for Arcane Bolt" + "(Sorcerer hurls a bolt towards his enemy dealing 100 - 150 Damage. This spell uses 40 mana and generates two Arcane Stacks.)");
            Console.WriteLine("Press 3 for Death And Decay" + "(Sorcerer casts a debuff on his opponent dealing 75 - 90 damage for four rounds. This spell uses 100 Mana.)");
            Console.WriteLine("Press 4 for Eye of an Eye" + "(Steals Mana from Enemy and adds it to Sorcerers. Also This spell deals 20 - 60 Damage to the opponent. This spell is free.)");
            Console.WriteLine("Press 5 for Arcane Explosions" + "(Sorcerer deals damage to his Enemy using his generated Arcane Stacks. This Spell only does bonus damage if Sorcerer has generated two or three Arcane Stacks, if not, it deals 150 - 200 damage. If generated, this spell deals 150 - 200 times two for two Stacks or 150 - 200 times three for three Generated Arcnae Stacks. Costs 90 Mana.)");
            Console.WriteLine("*************************************");
            Console.WriteLine("Cast Your Spell: ");
        }

        public override SpellReport SpellOne(Hero opponent)
        {
            Console.WriteLine("Casting Basic Attack!");
            int damage = random.Next(MinDamage, MaxDamage);
			arcaneStacks = arcaneStacks < 3 ? arcaneStacks += 1 : 3;
			opponent.CurrentHealth -= damage;
            Console.WriteLine(Name + " Hits for " + damage + " Damage");
            Console.WriteLine(opponent.Name + " Remaining Health: " + opponent.CurrentHealth);
            DamageDone += damage;
            opponent.DamageTaken += damage;

            return SpellReport.SUCCESS;
        }

        public override SpellReport SpellTwo(Hero opponent)
        {
            int damage = random.Next(100, 150);
            if (CurrentMana >= 40)
            {
				arcaneStacks = arcaneStacks < 2 ? arcaneStacks += 2 : 3;
                Console.WriteLine("Casting Arcane Bolt");
                opponent.CurrentHealth -= damage;
                CurrentMana -= 40;
                Console.WriteLine(Name + " Hits for " + damage + " Damage");
                Console.WriteLine(opponent.Name + " Remaining Health: " + opponent.CurrentHealth);
                DamageDone += damage;
                opponent.DamageTaken += damage;

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }

        public override SpellReport SpellThree(Hero opponent)
        {
            if (CurrentMana >= 100)
            {
                Console.WriteLine("Casting Death and Decay");
                opponent.Debuffs.Add(new DeathAndDecay());
                CurrentMana -= 100;

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }

        public override SpellReport SpellFour(Hero opponent)
        {
            Console.WriteLine("Casting Eye for an Eye");
            int damage, manaSteal;
            damage = random.Next(20, 60);
            manaSteal = random.Next(40, 60);
            opponent.CurrentMana = opponent.CurrentMana - manaSteal <= 0 ?  0 : opponent.CurrentMana -= manaSteal;
            CurrentMana = CurrentMana + manaSteal > MaxMana ? MaxMana : CurrentMana += manaSteal;
            opponent.CurrentHealth = CurrentHealth - damage <= 0 ? 0 : opponent.CurrentHealth -= damage;
            Console.WriteLine("Sorcerer Stole " + manaSteal + " from " + opponent.Name + " !");
            Console.WriteLine("Sorcerer Dealt " + damage + " to " + opponent.Name + " !");
            Console.WriteLine("Sorcerer's Mana: " + CurrentMana);
            DamageDone += damage;
            opponent.DamageTaken += damage;

            return SpellReport.SUCCESS;

        }

        public override SpellReport SpellFive(Hero opponent)
        {
            int damage;
            if (CurrentMana >= 90)
            {
                Console.WriteLine("Casting Arcane Explosions");
                if(arcaneStacks == 2)
                {
                    damage = random.Next(150, 200) * arcaneStacks;
                    opponent.CurrentHealth -= damage;
                    Console.WriteLine(Name + " Hits for " + damage + " Damage");
                    arcaneStacks -= 2;
                    CurrentMana -= 90;
                    DamageDone += damage;
                    opponent.DamageTaken += damage;
                }
                else if (arcaneStacks == 3)
                {
                    damage = random.Next(150, 200) * arcaneStacks;
                    opponent.CurrentHealth -= damage;
                    Console.WriteLine(Name + " Hits for " + damage + " Damage");
                    arcaneStacks -= 3;
                    CurrentMana -= 90;
                    DamageDone += damage;
                    opponent.DamageTaken += damage;
                }
                else
                {
                    damage = random.Next(150, 200) * arcaneStacks;
                    opponent.CurrentHealth -= damage;
                    Console.WriteLine(Name + " Hits for " + damage + " Damage");
                    CurrentMana -= 90;
                    DamageDone += damage;
                    opponent.DamageTaken += damage;
                }
                Console.WriteLine(opponent.Name + " Current Health: " + opponent.CurrentHealth);

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }
    }
}
