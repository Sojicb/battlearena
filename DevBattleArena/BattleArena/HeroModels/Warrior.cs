﻿using BattleArena.Buffs;
using BattleArena.Buffs.Warriorbuffs;
using BattleArena.DataAccess.Models;
using BattleArena.Debuffs;
using System;
using System.Collections.Generic;

namespace BattleArena.HeroModels
{
    public class Warrior : Hero
    {
        Random random = new Random();
        private double CurrentRage;
        private  int MaxRage = 100;

        public Warrior(HeroModel heroModels)
        {
            Id = heroModels.Id;
            Name = heroModels.Name;
            MaxHealth = heroModels.Health;
            CurrentHealth = heroModels.Health;
            MaxMana = heroModels.Mana;
            CurrentMana = 0;
            MaxDamage = heroModels.MaxDamage;
            MinDamage = heroModels.MinDamage;
            Buffs = new List<Buff>();
            Debuffs = new List<Debuff>();
        }
        public override SpellReport CastSpell(Hero opponent, bool isRecasted = false)
        {
            if (!isRecasted)
            {
                GenerateRage();
            }
            DisplayStats();
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    return SpellOne(opponent);
                case 2:
                    return SpellTwo(opponent);
                case 3:
                    return SpellThree(opponent);
                case 4:
                    return SpellFour(opponent);
                case 5:
                    return SpellFive(opponent);
            }
            return SpellReport.FAILURE;
        }

        public override SpellReport CastSpellAI(Hero opponent, bool isRecasted = false, int key = 0)
        {
            if (!isRecasted)
            {
                GenerateRage();
            }
            DisplayStats();
            switch (key)
            {
                case 1:
                    return SpellOne(opponent);
                case 2:
                    return SpellTwo(opponent);
                case 3:
                    return SpellThree(opponent);
                case 4:
                    return SpellFour(opponent);
                case 5:
                    return SpellFive(opponent);
            }
            return SpellReport.FAILURE;
        }

        private void DisplayStats()
        {
            Console.WriteLine("\n\nName: " + Name);
            Console.WriteLine("Health: " + CurrentHealth);
            Console.WriteLine("Mana: " + CurrentMana);
            Console.WriteLine("Rage: " + CurrentRage);
            Console.WriteLine("*************************************");
            Console.WriteLine("Press 1 for Basic Attack" + "(Deals Damage between " + MinDamage + " and " + MaxDamage + " to an Opponent. It requires no mana and generates 15 - 25 Rage per Attack.)");
            Console.WriteLine("Press 2 for Heroic Strike" + "(Warrior Strikes an Enemy dealing 90 - 150 damage and healing for 60 - 100 Health. This Attack generates 20 Rage.)");
            Console.WriteLine("Press 3 for Whirlwind" + "(Warrior Spins three times and hits an enemy for combined damage of (maximum : 180). This Spell Uses 30 Rage.)");
            Console.WriteLine("Press 4 for Last Stand" + "(Last Stand is Warriors protection spell. Warrior Heals for 200 Health and Clears all Debuffs that are cast on him. This Spell Uses 50 Rage.)");
            Console.WriteLine("Press 5 for Decimate" + "(Depending on Warriors Health, Warriors Basic Attack Damage is Doubled if Warrior is above 75% Health, Trippled if he is above 50% Healt, or Quadrupled if he is above 25% Health dealing maximum amount of 400 Damage. This Spell Uses 90 Rage.)");
            Console.WriteLine("*************************************");
            Console.WriteLine("Cast Your Spell: ");
        }

        private void GenerateRage()
        {
            CurrentRage = (CurrentRage + 20 > MaxRage) ? CurrentRage = MaxRage : CurrentRage += 20;
        }

        public override SpellReport SpellOne(Hero opponent)
        {
            Console.WriteLine("Casting Basic Attack");
            int RageGenerate = random.Next(15, 25);
            int damage = random.Next(MinDamage, MaxDamage);
            CurrentRage = (CurrentRage + RageGenerate > MaxRage) ? CurrentRage = MaxRage : CurrentRage += RageGenerate;
            opponent.CurrentHealth -= damage;
            Console.WriteLine(Name + " Hits for " + damage);
            Console.WriteLine(opponent.Name + " Remaining Health: " + opponent.CurrentHealth);
            DamageDone += damage;
            opponent.DamageTaken += damage;

            return SpellReport.SUCCESS;
        }

        public override SpellReport SpellTwo(Hero opponent)
        {
            Console.WriteLine("Casting Heroic Strike");
            int heal = random.Next(MinDamage, MaxDamage);
            int damage = random.Next(90, 150);
            CurrentRage = CurrentRage + 20 > MaxRage ? MaxRage : CurrentRage + 20;
            CurrentHealth = CurrentHealth + heal > MaxHealth ? MaxHealth : CurrentHealth + heal;
            opponent.CurrentHealth -= damage;
            Console.WriteLine(Name + " Hits for " + damage);
            Console.WriteLine(Name + " Heals for " + heal);
            Console.WriteLine(opponent.Name + " Remaining Health: " + opponent.CurrentHealth);
            DamageDone += damage;
            HealingDone += heal;
            opponent.DamageTaken += damage;

            return SpellReport.SUCCESS;
        }

        public override SpellReport SpellThree(Hero opponent)
        {
            if (CurrentRage >= 30)
            {
                int StrikeOne, StrikeTwo, StrikeThree;
                StrikeOne = random.Next(20, 60);
                StrikeTwo = random.Next(30, 60);
                StrikeThree = random.Next(40, 60);
                int DamageSum = StrikeThree + StrikeTwo + StrikeOne;
                Console.WriteLine("Warrior Hits for: " + StrikeOne + "/" + StrikeTwo + "/" + StrikeThree + " Damage in three spins!" + "(" + DamageSum + " Damage" + ")");
                CurrentRage -= 30;
                opponent.CurrentHealth -= DamageSum;
                Console.WriteLine(opponent.Name + " Health Remaining: " + opponent.CurrentHealth);
                DamageDone += DamageSum;
                opponent.DamageTaken += DamageSum;

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }

        public override SpellReport SpellFour(Hero opponent)
        {
            if(CurrentRage >= 50)
            {
                Console.WriteLine("Casting Last Stand");
                new ClearDebuffs().Action(this);
                CurrentHealth += 200;
                CurrentRage -= 50;
                Console.WriteLine("Warriors Current Health: " + CurrentHealth);
                HealingDone += 200;

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }

        public override SpellReport SpellFive(Hero opponent)
        {
            if (CurrentRage >= 90)
            {
                int damage;
                Console.WriteLine("Casting Decimate");
                if (CurrentHealth > ((double)MaxHealth / 100) * 75)
                {
                    damage = random.Next(MinDamage, MaxDamage) * 2;
                    Console.WriteLine("Warrior Decimates the enemy for " + damage);
                    opponent.CurrentHealth -= damage;
                    DamageDone += damage;
                    opponent.DamageTaken += damage;
                }
                else if (CurrentHealth > ((double)MaxHealth / 100) * 50)
                {
                    damage = random.Next(MinDamage, MaxDamage) * 3;
                    Console.WriteLine("Warrior Decimates the enemy for " + damage);
                    opponent.CurrentHealth -= damage;
                    DamageDone += damage;
                    opponent.DamageTaken += damage;
                }
                else if (CurrentHealth < ((double)MaxHealth / 100) * 50)
                {
                    damage = random.Next(MinDamage, MaxDamage) * 4;
                    Console.WriteLine("Warrior Decimates the enemy for " + damage);
                    opponent.CurrentHealth -= damage;
                    DamageDone += damage;
                    opponent.DamageTaken += damage;
                }
                CurrentRage -= 90;
                Console.WriteLine(opponent.Name + " Current Health: " + opponent.CurrentHealth);

                return SpellReport.SUCCESS;
            }

            return SpellReport.FAILURE;
        }
    }
}
