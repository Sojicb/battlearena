﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.HeroModels
{
    public interface IHero
    {
        void SpellOne();
        void SpellTwo();
        void SpellThree();
        void SpellFour();
        void SpellFive();
    }
}
