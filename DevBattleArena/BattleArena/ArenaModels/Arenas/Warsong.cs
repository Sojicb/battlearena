﻿using BattleArena.Buffs;
using BattleArena.DataAccess.Models;
using BattleArena.Debuffs;
using BattleArena.Debuffs.Warsong;
using System.Collections.Generic;

namespace BattleArena.ArenaModels.Arenas
{
    public class Warsong : Arena
    {
        public Warsong(ArenaModel arenaModels)
        {
            Id = arenaModels.Id;
            Name = arenaModels.Name;
            BuffOne = arenaModels.BuffOne;
            DebuffOne = arenaModels.DebuffOne;
            BattleMode = arenaModels.BattleMode;
            Description = arenaModels.Description;
            Buffs = new List<Buff>();
            Debuffs = new List<Debuff>();
        }
        public override void Activate(Hero opponent)
        {
            new FallingRocks().Action(opponent);
            new HealthRegen().Action(opponent);
        }
    }
}
