﻿using BattleArena.Buffs;
using BattleArena.Buffs.TigersPeak;
using BattleArena.DataAccess.Models;
using BattleArena.Debuffs;
using BattleArena.Debuffs.TigersPeak;
using System.Collections.Generic;

namespace BattleArena.ArenaModels.Arenas
{
    public class TigersPeak : Arena
    {
        public TigersPeak(ArenaModel arenaModels)
        {
            Id = arenaModels.Id;
            Name = arenaModels.Name;
            BuffOne = arenaModels.BuffOne;
            DebuffOne = arenaModels.DebuffOne;
            BattleMode = arenaModels.BattleMode;
            Description = arenaModels.Description;
            Buffs = new List<Buff>();
            Debuffs = new List<Debuff>();
        }
        public override void Activate(Hero opponent)
        {
            opponent.Debuffs.Add(new DullEdge());
        }
    }
}
