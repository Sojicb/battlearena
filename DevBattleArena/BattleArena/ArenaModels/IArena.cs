﻿namespace BattleArena.ArenaModels
{
    public interface IArena
    {
        void ActivateDebuff(Hero opponent);
    }
}
