﻿using BattleArena.Buffs;
using BattleArena.Debuffs;
using System.Collections.Generic;

namespace BattleArena.ArenaModels
{
    public abstract class Arena
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BuffOne { get; set; }
        public int DebuffOne { get; set; }
        public string BattleMode { get; set; }
        public string Description { get; set; }
        public List<Buff> Buffs { get; set; }
        public List<Debuff> Debuffs { get; set; }

        public abstract void Activate(Hero opponent);

    }
}
