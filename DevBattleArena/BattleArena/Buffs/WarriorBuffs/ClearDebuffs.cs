﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.Buffs.Warriorbuffs
{
    public class ClearDebuffs : Buff
    {
        public override void Action(Hero caster)
        {
            Console.WriteLine("\n" + caster.Name + " Cleared All Debuffs");
            caster.Debuffs.Clear();
        }
    }
}
