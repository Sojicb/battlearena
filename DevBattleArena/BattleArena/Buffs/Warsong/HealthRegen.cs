﻿namespace BattleArena.Buffs
{
    public class HealthRegen : Buff
    {
        public override void Action(Hero player)
        {
            player.CurrentHealth = player.CurrentHealth + 50 > player.MaxHealth ? player.MaxHealth : player.CurrentHealth + 50;
            System.Console.WriteLine("\nArena Healt Regenerate Buff Ticked " + player.Name + " Current Health: " + player.CurrentHealth);
        }
    }
}
