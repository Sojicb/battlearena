﻿namespace BattleArena.Buffs.PaladinBuffs
{
    public class Rejuvenate : Buff
    {
        public Rejuvenate()
        {
            Duration = 3;
            CurrentTick = 0;
        }
        public override void Action(Hero caster)
        {
            CurrentTick++;
            caster.CurrentHealth = caster.CurrentHealth + 50 > caster.MaxHealth ? caster.MaxHealth : caster.CurrentHealth + 50;
            System.Console.WriteLine(caster.Name + " Current Health: " + caster.CurrentHealth);
            System.Console.WriteLine("\nPaladins Rejuvenate Ticked " + CurrentTick);
            caster.HealingDone += 50;
        }
    }
}
