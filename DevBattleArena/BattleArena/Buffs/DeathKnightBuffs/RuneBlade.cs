﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.Buffs.DeathKnightBuffs
{
    public class RuneBlade : Buff
    {
        public RuneBlade()
        {
            Duration = 3;
            CurrentTick = 0;
        }
        public override void Action(Hero caster)
        {
            caster.MinDamage = 175;
            caster.MaxDamage = 195;
            Console.WriteLine("\nDeath Knights Minimum Damage: " + caster.MinDamage);
            Console.WriteLine("Death Knights Maximum Damage: " + caster.MaxDamage);
            CurrentTick++;
            Console.WriteLine("\nBloody Runeblade Current tick: " + CurrentTick);
            if(CurrentTick > 3)
            {
                caster.MinDamage = 100;
                caster.MaxDamage = 120;
            }
        }
    }
}
