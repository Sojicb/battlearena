﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleArena.HeroModels;

namespace BattleArena.Buffs.DeathKnightBuffs
{
    public class BloodRunes : Buff
    {

        private DeathKnight deathKnight;
        public BloodRunes(DeathKnight deathKnight)
        {
            Duration = 3;
            CurrentTick = 0;
            this.deathKnight = deathKnight;
        }


        public override void Action(Hero caster)
        {
            Console.WriteLine("\nDeath Knigt Ganed One Rune this Round and 50 Health");
            caster.CurrentHealth = caster.CurrentHealth + 50 > caster.MaxHealth ? caster.MaxHealth : caster.CurrentHealth + 50;
            Console.WriteLine("Death Knight Health: " + caster.CurrentHealth);
            if(deathKnight.Runes < 5)
            {
                deathKnight.Runes++;
            }
            Console.WriteLine("\nDeath Knight Runes: " + deathKnight.Runes);
            caster.HealingDone += 50;
            CurrentTick++;
            Console.WriteLine("BloodRunes Buff Ticked, Current tick: " + CurrentTick);
        }
    }
}
