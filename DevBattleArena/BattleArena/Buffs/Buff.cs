﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.Buffs
{
    public abstract class Buff
    {
        public int Duration { get; set; }
        public int CurrentTick { get; set; }
        public abstract void Action(Hero caster);
    }
}
