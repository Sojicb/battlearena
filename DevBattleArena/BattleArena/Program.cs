﻿using BattleArena.GameModes;

namespace BattleArena
{
    public class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game(ChooseMode.ChooseGameMode());
            game.Start();
        }
    }
}
