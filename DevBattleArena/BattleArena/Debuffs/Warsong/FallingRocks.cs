﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.Debuffs.Warsong
{
    public class FallingRocks : Debuff
    {
        int hitChance;
        public override void Action(Hero player)
        {
            Random random = new Random();
            hitChance = random.Next(1, 30);
            if(hitChance > 20)
            {
                player.CurrentHealth -= 50 ;
                Console.WriteLine("\n" + player.Name + " Got hit by a falling stone brick for " + 50 + " Damage");
                Console.WriteLine(player.Name + "Current Health: " + player.CurrentHealth);
                hitChance = 0;
            }
            else
            {
                return;
            }
        }
    }
}
