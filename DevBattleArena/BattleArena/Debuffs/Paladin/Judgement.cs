﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.Debuffs.Paladin
{
    public class Judgement : Debuff
    {
        public Judgement()
        {
            Duration = 3;
            CurrentTick = 0;
        }
        public override void Action(Hero opponent)
        {
            opponent.DamageTaken += 15;
            opponent.CurrentHealth -= 15;
            Console.WriteLine("\n" + opponent.Name + " Healt reduced by 15 from Judgement!");
            Console.WriteLine(opponent.Name + " Current Health : " + opponent.CurrentHealth);
            CurrentTick++;
            Console.WriteLine("Judgement Current Tick: " + CurrentTick);
            
        }
    }
}
