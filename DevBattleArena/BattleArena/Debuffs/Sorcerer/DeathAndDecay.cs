﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.Debuffs.Sorcerer
{
    public class DeathAndDecay : Debuff
    {
        Random random = new Random();

        public DeathAndDecay()
        {
            Duration = 4;
            CurrentTick = 0;
        }

        public override void Action(Hero opponent)
        {
            CurrentTick++;
            int damage = random.Next(75, 90);
            opponent.CurrentHealth -= damage;
            Console.WriteLine("\nDeath And Decay hits " + opponent.Name + " For " + damage + " . " + opponent.Name + " Current Health: " + opponent.CurrentHealth);
            Console.WriteLine("Current Tick: " + CurrentTick);
            opponent.DamageTaken += damage;
        }

    }
}
