﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.Debuffs.DeathKnightDebuff
{
    public class Bleed : Debuff
    {
        public Bleed()
        {
            Duration = 3;
            CurrentTick = 0;
        }
        public override void Action(Hero opponent)
        {
            opponent.CurrentHealth -= 20;
            Console.WriteLine("\n" + opponent.Name + " Healt reduced by 20 from Death Strike!");
            Console.WriteLine(opponent.Name + " Current Health : " + opponent.CurrentHealth);
            CurrentTick++;
            Console.WriteLine("Death Strike Current Tick: " + CurrentTick);
            opponent.DamageTaken += 20;
        }
    }
}
