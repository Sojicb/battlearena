﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArena.Debuffs.TigersPeak
{
    public class DullEdge : Debuff
    {
        public override void Action(Hero player)
        {
            Console.WriteLine("\n" + player.Name + "Damage reduced by One. Tiger's Peak Debuff ticked.");
            player.MaxDamage = player.MaxDamage < 10 ? 10 : player.MaxDamage--;
            player.MinDamage = player.MinDamage < 10 ? 10 : player.MinDamage--;
        }
    }
}
