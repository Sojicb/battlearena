﻿using BattleArena.GameModes;

namespace BattleArena
{
    public class Game : IGame
    {
        public Game(IGameMode gameMode)
        {
            GameMode = gameMode;
        }

        public IGameMode GameMode { get; }

        public void Start()
        {
            GameMode.PlayGame();
        }
    }
}
