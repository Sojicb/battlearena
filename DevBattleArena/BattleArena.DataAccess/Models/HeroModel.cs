﻿namespace BattleArena.DataAccess.Models
{
    public class HeroModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Health { get; set; }
        public int Mana { get; set; }
        public int MinDamage { get; set; }
        public int MaxDamage { get; set; }
    }
}
