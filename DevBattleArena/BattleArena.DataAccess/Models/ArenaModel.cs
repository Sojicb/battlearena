﻿namespace BattleArena.DataAccess.Models
{
    public class ArenaModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BuffOne { get; set; }
        public int DebuffOne { get; set; }
        public string BattleMode { get; set; }
        public string Description { get; set; }
    }
}
