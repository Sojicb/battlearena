﻿using BattleArena.DataAccess.Repositories;

namespace BattleArena.DataAccess
{
    public class DataBaseAccess
    {
        private string connectionString = "Data Source=(local)\\SQLExpress;Initial Catalog=BattleArena;Integrated Security=True";
        public HeroRepository HeroRepository => new HeroRepository(connectionString);
        public ArenaRepository ArenaRepository => new ArenaRepository(connectionString);
    }
}
