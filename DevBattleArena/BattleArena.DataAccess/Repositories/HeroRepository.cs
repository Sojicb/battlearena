﻿using BattleArena.DataAccess.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BattleArena.DataAccess.Repositories
{
    public class HeroRepository
    {
        private readonly string _connectionString;
        public HeroRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Add(HeroModel model)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "INSERT INTO Hero(Name, MaxHealth, MaxMana, MaxDamage, MinDamage) OUTPUT Inserted.Id " + 
                        "VALUES(@Name, @MaxHealth, @MaxMana, @MaxDamage, @MinDamage)";
                    sqlCommand.Parameters.AddWithValue("@Name", model.Name);
                    sqlCommand.Parameters.AddWithValue("@MaxHealth", model.Health);
                    sqlCommand.Parameters.AddWithValue("@MaxMana", model.Mana);
                    sqlCommand.Parameters.AddWithValue("@MinDamage", model.MinDamage);
                    sqlCommand.Parameters.AddWithValue("@MaxDamage", model.MaxDamage);

                    return (int)sqlCommand.ExecuteScalar();
                }
            }
        }

        public string Delete(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "DELETE FROM Hero OUTPUT deleted.Name WHERE Id=@Id";
                    sqlCommand.Parameters.AddWithValue("@Id", id);
                    sqlCommand.ExecuteNonQuery();

                    return sqlCommand.ExecuteScalar() as string;
                }
            }
        }

        public int Update(HeroModel model)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "UPDATE Hero SET Name=@Name, MaxHealth=@Health, MaxMana=@Mana, MaxDamage=@MaxDamage, " +
                        "MinDamage=@MinDamage OUTPUT Inserted.Id where Id=@Id";
                    sqlCommand.Parameters.AddWithValue("@Name", model.Name);
                    sqlCommand.Parameters.AddWithValue("@Health", model.Health);
                    sqlCommand.Parameters.AddWithValue("@Mana", model.Mana);
                    sqlCommand.Parameters.AddWithValue("@MinDamage", model.MinDamage);
                    sqlCommand.Parameters.AddWithValue("@MaxDamage", model.MaxDamage);
                    sqlCommand.Parameters.AddWithValue("@Id", model.Id);
                    sqlCommand.ExecuteNonQuery();

                    return (int)sqlCommand.ExecuteScalar();
                }
            }
        }

        public HeroModel Get(int id)
        {
            HeroModel model = new HeroModel();

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "SELECT * FROM Hero WHERE Id = @Id";
                    sqlCommand.Parameters.AddWithValue("@Id", id);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            model.Id = (int)reader["Id"];
                            model.Name = reader["Name"] as string;
                            model.Health = (int)reader["MaxHealth"];
                            model.Mana = (int)reader["MaxMana"];
                            model.MinDamage = (int)reader["MinDamage"];
                            model.MaxDamage = (int)reader["MaxDamage"];
                        }
                    }
                }
            }
            return model;
        }

        public List<HeroModel> GetHeroList()
        {
            List<HeroModel> heroes = new List<HeroModel>();

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "SELECT * FROM Hero";

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            heroes.Add(new HeroModel
                            {
                                Id = (int)reader["Id"],
                                Name = reader["Name"] as string,
                                Health = (int)reader["MaxHealth"],
                                Mana = (int)reader["MaxMana"],
                                MinDamage = (int)reader["MinDamage"],
                                MaxDamage = (int)reader["MaxDamage"],
                            });
                        }
                    }
                }
            }
            return heroes;
        }
    }
}
