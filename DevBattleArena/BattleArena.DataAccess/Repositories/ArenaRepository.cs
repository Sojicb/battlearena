﻿using BattleArena.DataAccess.Models;
using System.Data.SqlClient;

namespace BattleArena.DataAccess.Repositories
{
    public class ArenaRepository
    {
        private readonly string _connectionString;
        public ArenaRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ArenaModel Get(string name)
        {
            ArenaModel model = new ArenaModel();

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "SELECT * FROM Arena WHERE Name = @Name";
                    sqlCommand.Parameters.AddWithValue("@Name", name);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            model.Id = (int)reader["Id"];
                            model.Name = reader["Name"] as string;
                            model.BuffOne = (int)reader["BuffOne"];
                            model.DebuffOne = (int)reader["DebuffOne"];
                            model.BattleMode = reader["BattleMode"] as string;
                            model.Description = reader["Description"] as string;
                        }
                    }
                }
            }
            return model;
        }
    }
}
